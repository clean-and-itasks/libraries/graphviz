# graphviz

This library provides tools to create graphviz graphs (dot).

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
