# Changelog

#### 1.0.2

- Chore: support base `3.0`.

#### 1.0.1

- Fix: `toString` of edge styles.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  graphviz modules.
